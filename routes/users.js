var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});


/*
 * GET userlist.
 */
router.get('/userlist', function(req, res) {
  var db = req.db;
  //usercollection = table name
  var collection = db.get('usercollection');
  collection.find({},{},function(e,docs){
    res.json(docs);
  });
});


/*
 * GET user.
 */
router.get('/login/:email/:password', function(req, res) {
  var db = req.db;
  var email = req.params.email;
  var password = req.params.password;
  var collection = db.get('usercollection');
  collection.find({"email" : email, "password" : password},{},function(e,docs){
    res.json(docs);
  });
});


module.exports = router;
